package com.example.permissionsexample

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    btnCameraPermission.setOnClickListener{
        if(ContextCompat.checkSelfPermission(this,Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            Toast.makeText(this,"You already have the permission for camera and gps",Toast.LENGTH_SHORT).show()
        }else{
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.CAMERA , Manifest.permission.ACCESS_FINE_LOCATION) , CAMERA_AND_FINE_LOCATION_PERMISSION_CODE)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == CAMERA_PERMISSION_CODE){
            if(grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Toast.makeText(this,
                    "You already have the permission",
                    Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this,
                    "Oops u just denied the permission for camera. You can also allow it from settings",
                    Toast.LENGTH_SHORT).show()
            }
        }
    }

    companion object{
        private const val CAMERA_PERMISSION_CODE = 1
        private const val FINE_LOCATION_PERMISSION_CODE = 1
        private const val CAMERA_AND_FINE_LOCATION_PERMISSION_CODE = 12

    }
}